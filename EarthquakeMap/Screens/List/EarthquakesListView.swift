//
//  EarthquakesListView.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import UIKit

class EarthquakesListView: UIView {

    lazy var tableView: UITableView = {
        let view = UITableView()

        view.backgroundColor = .clear
        view.separatorStyle = .none
        view.registerCell(EarthquakeListCell.self)
        view.rowHeight = 120

        return view
    }()

    init() {
        super.init(frame: CGRect.zero)

        backgroundColor = ColorPallete.backgroundWhite

        addSubview(tableView)
        tableView.fillSuperview()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
