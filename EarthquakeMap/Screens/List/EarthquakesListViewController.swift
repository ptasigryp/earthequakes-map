//
//  ListViewController.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import UIKit

class EarthquakesListViewController: UIViewController {

    private let myView: EarthquakesListView
    private let viewModel: EarthquakesListViewModel

    override func viewDidLoad() {
        myView.tableView.delegate = viewModel
        myView.tableView.dataSource = viewModel

        viewModel.load()
    }

    override func loadView() {
        view = myView
    }

    init(viewModel: EarthquakesListViewModel) {
        self.viewModel = viewModel
        self.myView = EarthquakesListView()
        super.init(nibName: nil, bundle: nil)

        viewModel.viewDelegate = self
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension EarthquakesListViewController: EarthquakesListViewModelViewDelegate {
    func dataLoaded() {
        myView.tableView.reloadData()
    }

    func busy() {
        myView.isBusy = true
    }

    func idle() {
        myView.isBusy = false
    }

    func showError(model: AlertViewModel) {
        showAlert(using: model)
    }
}
