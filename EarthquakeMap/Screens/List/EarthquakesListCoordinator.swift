//
//  ListCoordinator.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import UIKit

class EarthquakesListCoordinator: ChildCoordinator {

    let appContext: AppContext
    weak var parentCoordinator: ParentCoordinator?
    let viewController: UIViewController

    func start() {}

    init(context: AppContext) {
        self.appContext = context

        let viewModel = EarthquakesListViewModel(earthquakesService: context.earthquakesService)
        self.viewController = EarthquakesListViewController(viewModel: viewModel)

        viewController.tabBarItem = UITabBarItem(title: "List",
                                                 image: UIImage(named: "city"), // TODO: - Change icon
                                                 selectedImage: UIImage(named: "city"))
    }
}
