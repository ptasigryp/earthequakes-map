//
//  EarthquakeListCell.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import UIKit

struct EarthquakeListCellModel {
    //swiftlint:disable:next identifier_name
    let id: String
    let mag: Double
    let time: Date
    let felt: Int?
    let tsunami: Int
}

class EarthquakeListCell: UITableViewCell, ReusableView {

    private lazy var propertiesStackView: UIStackView = {
        let stackView = UIStackView()

        stackView.axis = .vertical
        stackView.distribution = .fillEqually

        return stackView
    }()

    private func setupViews() {
        addSubview(propertiesStackView)
        propertiesStackView.fillSuperview()
        backgroundColor = .clear
    }

    func configure(with viewModel: EarthquakeListCellModel) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current

        propertiesStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }

        let properties: [String: String] = [
            "ID": viewModel.id,
            "Magnitude": "\(viewModel.mag)",
            "Time": dateFormatter.string(from: viewModel.time),
            "Felt": viewModel.felt == nil ? "n/a" : "\(viewModel.felt!)",
            "Tsunami": "\(viewModel.tsunami)"
        ]

        let labels: [UILabel] = properties.map {
            let label = UILabel()

            label.text = "\($0.key): \($0.value)"
            label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium)
            label.textColor = .black
            label.numberOfLines = 1

            return label
        }
        labels.forEach {
            propertiesStackView.addArrangedSubview($0)
        }
    }

    private func commonInit() {
        selectionStyle = .none
        setupViews()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
}
