//
//  ListViewModel.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import UIKit

protocol EarthquakesListViewModelViewDelegate: class {
    func dataLoaded()
    func busy()
    func idle()
    func showError(model: AlertViewModel)
}

class EarthquakesListViewModel: NSObject {
    private let earthquakesService: EarthquakesService
    private var earthquakes: [Earthquake] = []
    weak var viewDelegate: EarthquakesListViewModelViewDelegate?

    func load() {
        viewDelegate?.busy()
        earthquakesService.getEarthquakes { [weak self] result in
            guard let self = self else { return }

            self.viewDelegate?.idle()
            switch result {
            case .success(let data):
                self.earthquakes = data.features
                self.viewDelegate?.dataLoaded()
            case .failure(let error):
                let model = AlertViewModel.tryAgainAlertModel(message: error.localizedDescription) { [weak self] in
                    self?.load()
                }
                self.viewDelegate?.showError(model: model)
            }
        }
    }

    init(earthquakesService: EarthquakesService) {
        self.earthquakesService = earthquakesService
        super.init()
    }
}

extension EarthquakesListViewModel: UITableViewDelegate, UITableViewDataSource {
    func itemModel(at indexPath: IndexPath) -> EarthquakeListCellModel {
        let earthquake = earthquakes[indexPath.row]
        let model = EarthquakeListCellModel(id: earthquake.properties.id,
                                            mag: earthquake.properties.mag,
                                            time: earthquake.properties.time,
                                            felt: earthquake.properties.felt,
                                            tsunami: earthquake.properties.tsunami)
        return model
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: EarthquakeListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let model = itemModel(at: indexPath)
        cell.configure(with: model)
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return earthquakes.count
    }
}
