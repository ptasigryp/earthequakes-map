//
//  EarthquakesMapView.swift
//  EarthquakeMap
//
//  Created by michal.mankus on 25/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import UIKit
import MapKit

class EarthquakesMapView: UIView {

    lazy var mapView: MKMapView = {
        let view = MKMapView()

        return view
    }()

    init() {
        super.init(frame: CGRect.zero)

        addSubview(mapView)
        mapView.fillSuperview()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
