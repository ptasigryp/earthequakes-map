//
//  MapViewModel.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import Foundation

protocol EarthquakesMapViewModelViewDelegate: class {
    func dataLoaded()
    func busy()
    func idle()
    func showError(model: AlertViewModel)
}

class EarthquakesMapViewModel: NSObject {
    private let earthquakesService: EarthquakesService
    private(set) var earthquakes: [Earthquake] = []
    weak var viewDelegate: EarthquakesMapViewModelViewDelegate?

    func load() {
        viewDelegate?.busy()
        earthquakesService.getEarthquakes { [weak self] result in
            guard let self = self else { return }

            self.viewDelegate?.idle()
            switch result {
            case .success(let data):
                self.earthquakes = data.features
                self.viewDelegate?.dataLoaded()
            case .failure(let error):
                let model = AlertViewModel.tryAgainAlertModel(message: error.localizedDescription) { [weak self] in
                    self?.load()
                }
                self.viewDelegate?.showError(model: model)
            }
        }
    }

    init(earthquakesService: EarthquakesService) {
        self.earthquakesService = earthquakesService
        super.init()
    }
}
