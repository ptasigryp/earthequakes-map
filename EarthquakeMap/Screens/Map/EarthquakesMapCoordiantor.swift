//
//  MapCoordiantor.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import UIKit

class EarthquakesMapCoordinator: ChildCoordinator {

    let appContext: AppContext
    weak var parentCoordinator: ParentCoordinator?
    let viewController: UIViewController

    func start() {}

    init(context: AppContext) {
        self.appContext = context

        let viewModel = EarthquakesMapViewModel(earthquakesService: context.earthquakesService)
        self.viewController = EarthquakesMapViewController(viewModel: viewModel)

        viewController.tabBarItem = UITabBarItem(title: "Map",
                                                 image: UIImage(named: "map"),
                                                 selectedImage: UIImage(named: "map"))
    }
}
