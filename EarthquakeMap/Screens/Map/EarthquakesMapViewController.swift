//
//  MapViewController.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import UIKit
import MapKit

class EarthquakesMapViewController: UIViewController {

    private let myView: EarthquakesMapView
    private let viewModel: EarthquakesMapViewModel
    private var zoneCircles: [MKCircle] = []

    private func createZones() {
        myView.mapView.removeOverlays(zoneCircles)
        zoneCircles.removeAll()
        zoneCircles.reserveCapacity(viewModel.earthquakes.count)

        zoneCircles = viewModel.earthquakes.map {
            MKCircle(center: $0.coordinates2D, radius: $0.radius)
        }
        myView.mapView.addOverlays(zoneCircles)
    }

    override func viewDidLoad() {
        viewModel.load()
    }

    override func loadView() {
        view = myView
    }

    init(viewModel: EarthquakesMapViewModel) {
        self.viewModel = viewModel
        self.myView = EarthquakesMapView()
        super.init(nibName: nil, bundle: nil)

        myView.mapView.delegate = self
        viewModel.viewDelegate = self
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension EarthquakesMapViewController: EarthquakesMapViewModelViewDelegate {
    func dataLoaded() {
        createZones()
    }

    func busy() {
        myView.isBusy = true
    }

    func idle() {
        myView.isBusy = false
    }

    func showError(model: AlertViewModel) {
        showAlert(using: model)
    }
}

extension EarthquakesMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circle = MKCircleRenderer(overlay: overlay)
        circle.strokeColor = .red
        circle.fillColor = UIColor.red.withAlphaComponent(0.1)
        circle.lineWidth = 1
        return circle
    }
}
