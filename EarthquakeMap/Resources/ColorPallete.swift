//
//  ColorPallete.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import UIKit

struct ColorPallete {
    static let backgroundWhite: UIColor = UIColor(r: 239, g: 239, b: 239)
    static let dimmBackground: UIColor = UIColor.black.withAlphaComponent(0.2)
    static let oceanBlue: UIColor = UIColor(r: 48, g: 86, b: 212)
    static let almostBlack: UIColor = UIColor(r: 20, g: 20, b: 20)
}
