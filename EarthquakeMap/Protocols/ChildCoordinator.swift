//
//  ChildCoordinator.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import Foundation

protocol ChildCoordinator: Coordinator {
    var parentCoordinator: ParentCoordinator? { get set }
}

extension ChildCoordinator {

    func removeFromParentCoordinator() {
        parentCoordinator?.remove(childCoordinator: self)
    }
}

extension ChildCoordinator where Self: ParentCoordinator {

    func removeFromParentCoordinator() {
        removeAllChildCoordinators()
        parentCoordinator?.remove(childCoordinator: self)
    }
}
