//
//  Coordinator.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import UIKit

protocol Coordinator: class {
    var appContext: AppContext { get }
    var viewController: UIViewController { get }
    func start()
}
