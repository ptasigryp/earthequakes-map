//
//  AppContext.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import Foundation

class AppContext {

    let earthquakesService: EarthquakesService

    init() {
        let earthquakesConfig = ApiConfiguration(baseURL: "https://docs.mapbox.com/mapbox-gl-js/assets/")
        let earthquakesNetworkService = NetworkSerivce(config: earthquakesConfig)
        earthquakesService = EarthquakesService(networkService: earthquakesNetworkService)
    }
}
