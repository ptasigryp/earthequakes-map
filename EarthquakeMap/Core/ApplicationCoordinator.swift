//
//  ApplicationCoordinator.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import UIKit

class ApplicationCoordinator: ParentCoordinator {

    let appContext: AppContext
    let window: UIWindow
    let viewController: UIViewController
    var childCoordinators: [Coordinator] = []

    init(window: UIWindow) {
        self.window = window
        appContext = AppContext()
        let tabBarController = UITabBarController()
        viewController = tabBarController

        let coordinators: [ChildCoordinator] = [EarthquakesMapCoordinator(context: appContext), EarthquakesListCoordinator(context: appContext)]
        coordinators.forEach { add(childCoordinator: $0) }
        tabBarController.setViewControllers(coordinators.map { $0.viewController }, animated: false)
    }

    func start() {
        window.rootViewController = viewController
        window.makeKeyAndVisible()
    }
}
