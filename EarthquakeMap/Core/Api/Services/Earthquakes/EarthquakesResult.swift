//
//  EarthquakesResult.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import Foundation
import MapKit

protocol Location {
    var coordinates2D: CLLocationCoordinate2D { get }
    var radius: Double { get }
}

struct Crs: Codable {
    struct Properties: Codable {
        let name: String
    }

    let type: String
    let properties: Properties
}

struct Earthquake: Codable, Location {
    struct Properties: Codable {
        //swiftlint:disable:next identifier_name
        let id: String
        let mag: Double
        let time: Date
        let felt: Int?
        let tsunami: Int
    }

    struct Geometry: Codable {
        let type: String
        let coordinates: [Double]
    }

    let type: String
    let properties: Properties
    let geometry: Geometry

    var coordinates2D: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: geometry.coordinates[0], longitude: geometry.coordinates[1])
    }
    var radius: Double {
        return geometry.coordinates[2] * 100 // TODO: - Remove magic number. No documentation about radius scale
    }
}

struct EarthquakesResult: Codable {
    let type: String
    let crs: Crs
    var features: [Earthquake]
}
