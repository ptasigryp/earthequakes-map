//
//  EarthquakesApiClient.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import Foundation

class EarthquakesService: ApiClient {

    enum SortType {
        case magnitude
        case none
    }

    enum Filter {
        case magnitude(min: Double, max: Double)
        case radius(min: Double, max: Double)
    }

    private var earthquakesCache: EarthquakesResult?

    private func fetchEarthquakes(completion: @escaping (RequestResult<EarthquakesResult>) -> Void) {
        let endpoint = "earthquakes.geojson"
        fetchJSONData(endpoint: endpoint, expectedType: EarthquakesResult.self, completion: completion)
    }

    private func transformedResult(earthquakes: EarthquakesResult,
                                   sortedBy: SortType,
                                   filters: [Filter]) -> EarthquakesResult {
        var transformedResult = earthquakes

        switch sortedBy {
        case .magnitude:
            transformedResult.features.sort {
                $0.properties.mag > $1.properties.mag
            }
        case .none:
            break
        }

        for filter in filters {
            switch filter {
            case .magnitude(let min, let max):
                transformedResult.features.removeAll {
                    (min...max).contains($0.properties.mag) == false
                }
            case .radius(let min, let max):
                transformedResult.features.removeAll {
                    (min...max).contains($0.radius) == false
                }
            }
        }

        return transformedResult
    }

    func getEarthquakes(forceRefresh: Bool = false,
                        sortedBy: SortType = .magnitude,
                        filters: [Filter] = [],
                        completion:  @escaping (RequestResult<EarthquakesResult>) -> Void) {
        if forceRefresh == false, let earthquakes = earthquakesCache {
            let transformed = self.transformedResult(earthquakes: earthquakes,
                                                     sortedBy: sortedBy,
                                                     filters: filters)
            completion(.success(transformed))
        } else {
            fetchEarthquakes { result in
                switch result {
                case .success(let earthquakes):
                    self.earthquakesCache = earthquakes
                    let transformed = self.transformedResult(earthquakes: earthquakes,
                                                             sortedBy: sortedBy,
                                                             filters: filters)
                    completion(.success(transformed))
                case .failure:
                    completion(result)
                }
            }
        }
    }

    override init(networkService: NetworkServiceProtocol) {
        super.init(networkService: networkService)
    }
}
