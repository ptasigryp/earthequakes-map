//
//  ApiEnums.swift
//  EarthquakeMap
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import Foundation

enum RequestResult<T> {
    case success(T)
    case failure(Error)
}

enum ServerError: Error {

    case invalidResponse
}
