# Earthquakes Demo iOS #

_EarthquakesMap_ for iOS (Apple iPhone)

# Getting Setup #

* Install Xcode
* Install Cocoapods
* Run `$ pod install`from the project root directory.
* Open EarthquakeMap.xcworkspace in Xcode

# Requirements: #
- Xcode 10.0
- CocoaPods

# Authors #

* [Michał Mańkus](mailto:michmankus@gmail.com)