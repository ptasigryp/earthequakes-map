//
//  EarthquakeMapTests.swift
//  EarthquakeMapTests
//
//  Created by Michał Mańkus on 24/03/2019.
//  Copyright © 2019 devgaming. All rights reserved.
//

import XCTest
@testable import EarthquakeMap

class EarthquakeMapTests: XCTestCase {

    func testURLGenerating() {
        let config = ApiConfiguration(baseURL: "http://test.com/")
        let generatedURL = config.generateUrl(endpoint: "myendpoint/test/")
        let expectedURL = "http://test.com/myendpoint/test/"
        XCTAssertEqual(generatedURL, expectedURL)
    }

    func testCorrectEarthquakesModel() {
        class EarthquakesNetworkServiceMock: NetworkServiceProtocol {
            var config: ApiConfiguration {
                return ApiConfiguration(baseURL: "")
            }

            func apiRequest(endpoint: String, completion: @escaping (RequestResult<Data>) -> Void) {
                guard let mockData = """
                {
                    "type": "FeatureCollection",
                    "crs": {
                        "type": "name",
                        "properties": {
                            "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
                        }
                    },
                    "features": [
                        {
                            "type": "Feature",
                            "properties": {
                                "id": "ak16994521",
                                "mag": 2.3,
                                "time": 1507425650893,
                                "felt": null,
                                "tsunami": 0
                            },
                            "geometry": {
                                "type": "Point",
                                "coordinates": [ -151.5129, 63.1016, 0.0 ]
                            }
                        }
                    ]
                }
                """.data(using: .utf8) else { XCTFail("Incorrect product JSON"); return; }

                completion(.success(mockData))
            }
        }

        let callbackExpectation = expectation(description: "Network service has callback")

        let networkServiceMock = EarthquakesNetworkServiceMock()
        let earthquakesService = EarthquakesService(networkService: networkServiceMock)
        earthquakesService.getEarthquakes(forceRefresh: true) { result in
            switch result {
            case .success:
                break
            case .failure:
                XCTFail("Request failed")
            }
            callbackExpectation.fulfill()
        }

        waitForExpectations(timeout: 1) { error in
            if error != nil {
                XCTFail("waitForExpectationsWithTimeout")
            }
        }

    }
}
